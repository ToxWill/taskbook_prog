﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Begin10
{
    class Program
    {
        static void Main(string[] args)
        {
            double a, b;

            Console.Write("Введите первое число - ");
            a = Convert.ToDouble(Console.ReadLine());
            a = Math.Abs(a);

            Console.Write("Введите второе число - ");
            b = Convert.ToDouble(Console.ReadLine());
            b = Math.Abs(b);

            Console.WriteLine("Сумма этих чисел - " + (a + b));
            Console.WriteLine("Разность этих чисел - " + (a - b));
            Console.WriteLine("Произведение этих чисел - " + (a * b));
            Console.WriteLine("Частное квадратов этих чисел - " + ((a * a) / (b * b)));

            Console.ReadKey(true);
        }
    }
}
