﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Begin12
{
    class Program
    {
        static void Main(string[] args)
        {
            double a, b, c, P;

            Console.Write("Введите сторону A прямоугольного треугольника - ");
            a = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите сторону B прямоугольного треугольника - ");
            b = Convert.ToDouble(Console.ReadLine());

            c = Math.Sqrt((a * a) + (b * b));

            P = a + b + c;

            Console.WriteLine("Гипотенуза прямоугольного треугольника = " + c);
            Console.WriteLine("Периметр прямогульного треугольника равна = " + P);

            Console.ReadKey(true);
        }
    }
}
