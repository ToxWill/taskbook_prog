﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Begin13
{
    class Program
    {
        static void Main(string[] args)
        {
            double R1, R2, S1, S2, S3, pi;

            Console.Write("Введите радиус большого круга - ");
            R1 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите радиус круга меньше - ");
            R2 = Convert.ToDouble(Console.ReadLine());

            pi = 3.14;

            S1 = pi * (Math.Pow(R1, 2));
            S2 = pi * (Math.Pow(R2, 2));

            S3 = S1 - S2;

            Console.WriteLine("Площадь большого круга = " + S1);
            Console.WriteLine("Площадь маленького круга = " + S2);
            Console.WriteLine("Плащдь кольца = " + S3);

            Console.ReadKey(true);
        }
    }
}
