﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Begin14
{
    class Program
    {
        static void Main(string[] args)
        {
            double pi, L, R, S;

            Console.Write("Введите длину окружности - ");
            L = Convert.ToDouble(Console.ReadLine());

            pi = 3.14;

            R = L / (2 * pi);
            S = pi * Math.Pow(R, 2);

            Console.WriteLine("Радиус круга  = " + R);
            Console.WriteLine("Площадь круга = " + S);

            Console.ReadKey(true);
        }
    }
}
