﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Begin15
{
    class Program
    {
        static void Main(string[] args)
        {
            double L, S, D, pi;

            Console.Write("Введите площадь круга - ");
            S = Convert.ToDouble(Console.ReadLine());

            pi = 3.14;

            D = Math.Sqrt((4 * S) / pi);
            L = D * pi;

            Console.WriteLine("Диаметр окружности = " + D);
            Console.WriteLine("Длина окружности = " + L);

            Console.ReadKey(true);
        }
    }
}
