﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Begin16
{
    class Program
    {
        static void Main(string[] args)
        {
            double x1, x2;

            Console.Write("Введите первую координату на числовой оси - ");
            x1 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите вторую координату на числовой оси - ");
            x2 = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Растояние между двумя точками равна = " + Math.Abs(x2 - x1));

            Console.ReadKey(true);
        }
    }
}
