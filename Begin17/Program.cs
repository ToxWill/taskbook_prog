﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Begin17
{
    class Program
    {
        static void Main(string[] args)
        {
            double A, B, C, AC, BC;

            Console.Write("Введите точку А числовой оси - ");
            A = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите точку B числовой оси - ");
            B = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите точку C числовой оси - ");
            C = Convert.ToDouble(Console.ReadLine());

            AC = Math.Abs(C - A);
            BC = Math.Abs(C - B);

            Console.WriteLine("Дина отрезка АС = " + AC);
            Console.WriteLine("Дина отрезка BС = " + BC);
            Console.WriteLine("Сумма отрезков AC и BC = " + Math.Abs(AC + BC));

            Console.ReadKey(true);
        }
    }
}
