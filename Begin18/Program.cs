﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Begin18
{
    class Program
    {
        static void Main(string[] args)
        {
            double A, B, C;

            Console.Write("Введите точку А числовой оси - ");
            A = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите точку B числовой оси - ");
            B = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите точку C между координат A и B - ");
            C = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Произведение длин отрезков AC и BC = " + Math.Abs(C - A) * Math.Abs(B - C));

            Console.ReadKey(true);
        }
    }
}
