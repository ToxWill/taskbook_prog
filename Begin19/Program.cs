﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Begin19
{
    class Program
    {
        static void Main(string[] args)
        {
            double x1, x2, y1, y2, S, P, a, b;

            Console.WriteLine("Введите координат первой точки прямоугольника: ");
            Console.Write("Введите значения X - ");
            x1 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите значения Y - ");
            y1 = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Введите координат второй точки прямоугольника: ");
            Console.Write("Введите значения X - ");
            x2 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите значения Y - ");
            y2 = Convert.ToDouble(Console.ReadLine());

            a = x2 - x1;
            b = y2 - y1;

            P = 2 * (a + b);
            S = a * b;

            Console.WriteLine("Периметр прямоугольника = " + P);
            Console.WriteLine("Площадь прямоугольника = " + S);

            Console.ReadKey(true);
        }
    }
}
