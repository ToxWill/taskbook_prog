﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Begin20
{
    class Program
    {
        static void Main(string[] args)
        {
            double x1, x2, y1, y2;

            Console.WriteLine("Введите координаты первой точки: ");
            Console.Write("Введите значения X - ");
            x1 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите значения Y - ");
            y1 = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Введите координаты второй точки: ");
            Console.Write("Введите значения X - ");
            x2 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите значения Y - ");
            y2 = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Расстояиние между двумя точками =  " + Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2)));

            Console.ReadKey(true);
        }
    }
}
