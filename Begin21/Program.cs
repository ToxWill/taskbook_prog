﻿using System;

namespace Begin21
{
    class Program
    {
        static void Main(string[] args)
        {
            double x1, x2, x3, y1, y2, y3, a, b, c, P, S;

            Console.WriteLine("Введите координаты первых вершин треугольника: ");
            Console.Write("Введите значения X - ");
            x1 = Convert.ToDouble(Console.ReadLine());
            Console.Write("Введите значения Y - ");
            y1 = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Введите координаты вторых вершин треугольника: ");
            Console.Write("Введите значения X - ");
            x2 = Convert.ToDouble(Console.ReadLine());
            Console.Write("Введите значения Y - ");
            y2 = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Введите координаты третьих вершин треугольника: ");
            Console.Write("Введите значения X - ");
            x3 = Convert.ToDouble(Console.ReadLine());
            Console.Write("Введите значения Y - ");
            y3 = Convert.ToDouble(Console.ReadLine());

            a = Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));

            b = Math.Sqrt(Math.Pow(x3 - x2, 2) + Math.Pow(y3 - y2, 2));

            c = Math.Sqrt(Math.Pow(x1 - x3, 2) + Math.Pow(y1 - y3, 2));

            P = (a + b + c) / 2;

            S = Math.Sqrt(P * (P - a) * (P - b) * (P - c));

            Console.WriteLine("A = " + a);
            Console.WriteLine("B = " + b);
            Console.WriteLine("C = " + c);

            Console.WriteLine("Периметр треугольника = " + P);
            Console.WriteLine("Площадь треугольика = " + S);

            Console.ReadKey(true);
        }
    }
}
