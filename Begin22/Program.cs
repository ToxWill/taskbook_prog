﻿using System;
using System.Threading.Tasks.Dataflow;

namespace Begin22
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b, x;

            Console.Write("Введите значения А = ");
            a = Convert.ToInt32(Console.ReadLine());

            Console.Write("Введите значения В = ");
            b = Convert.ToInt32(Console.ReadLine());

            x = a;
            a = b;
            b = x;

            Console.WriteLine("A = " + a);
            Console.WriteLine("B = " + b);

            Console.ReadKey(true);
        }
    }
}
