﻿using System;

namespace Begin23
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b, c, x1, x2;

            Console.Write("Введите значения А = ");
            a = Convert.ToInt32(Console.ReadLine());

            Console.Write("Введите значения В = ");
            b = Convert.ToInt32(Console.ReadLine());

            Console.Write("Введите значения С = ");
            c = Convert.ToInt32(Console.ReadLine());

            x1 = b;
            x2 = c;
            b = a;
            c = x1;
            a = x2;

            Console.WriteLine("A = " + a);
            Console.WriteLine("B = " + b);
            Console.WriteLine("C = " + c);

            Console.ReadKey(true);
        }
    }
}
