﻿using System;

namespace Begin25
{
    class Program
    {
        static void Main(string[] args)
        {
            double x, y;

            Console.WriteLine("Найти значение функции y = 3x^6 - 6x^2 - 7 при данном значении x");
            Console.Write("Введите значения Х - ");
            x = Convert.ToDouble(Console.ReadLine());

            y = (3 * Math.Pow(x, 6)) - (6 * Math.Pow(x, 2)) - 7;

            Console.WriteLine("Значения Y = " + y);
            Console.ReadKey(true);
        }
    }
}
