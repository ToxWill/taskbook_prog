﻿using System;

namespace Begin26
{
    class Program
    {
        static void Main(string[] args)
        {
            double x, y;

            Console.WriteLine("Найти значение функции y = 4 * (x - 3)^6 - 7 * (x - 3)^3 + 2 при данном значении x");
            Console.Write("Введите значения Х - ");
            x = Convert.ToDouble(Console.ReadLine());

            y = 4 * Math.Pow((x - 3), 6) - 7 * Math.Pow((x - 3), 3) + 2;

            Console.WriteLine("Значения Y = " + y);
            Console.ReadKey(true);
        }
    }
}
