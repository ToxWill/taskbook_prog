﻿using System;

namespace Begin27
{
    class Program
    {
        static void Main(string[] args)
        {
            double A, A2, A4, A8;
            
            Console.WriteLine("Дано число A. Вычислить A^8, используя вспомогательную переменную и три операции умножения. Для этого последовательно находить A^2, A^4, A^8. Вывести все найденные степени числа A");
            Console.Write("Введите число А - ");

            A = Convert.ToDouble(Console.ReadLine());

            A2 = A * A;
            A4 = A2 * A2;
            A8 = A4 *A4;

            Console.WriteLine("Значение A в степени 2 = " + A2);
            Console.WriteLine("Значение A в степени 4 = " + A4);
            Console.WriteLine("Значение A в степени 8 = " + A8);

            Console.ReadKey(true);
        }
    }
}
