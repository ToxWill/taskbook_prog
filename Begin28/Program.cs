﻿using System;

namespace Begin28
{
    class Program
    {
        static void Main(string[] args)
        {
            double A, B, X;
            
            Console.WriteLine("Дано число A. Вычислить A^15, используя две вспомогательные переменные и пять операций умножения. Для этого последовательно находить A^2, A^3, A^5, A^10, A^15. Вывести все найденные степени числа A");
            Console.Write("Введите число А - ");

            A = Convert.ToDouble(Console.ReadLine());

            B = A * A;
            Console.WriteLine("Значение A в степени 2 = " + B);

            X = B * A;
            Console.WriteLine("Значение А в степени 3 = " + X);

            B = X * (A * A);
            Console.WriteLine("Значение А в степени 5 = " + B);

            X = B * B;
            Console.WriteLine("Значение А в степени 10 = " + X);
            Console.WriteLine("Значение А в степени 15 = " + X * B);

            Console.ReadKey(true);
        }
    }
}
