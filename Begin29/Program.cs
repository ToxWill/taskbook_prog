﻿using System;

namespace Begin29
{
    class Program
    {
        static void Main(string[] args)
        {
            double pi, Grad, Rad;

            Console.Write("Ввдеите гардусную меру угла (0 <= X < 360)");
            Grad = Convert.ToDouble(Console.ReadLine());

            pi = 3.14;

            Rad = (pi * Grad) / 180;

            Console.WriteLine("Угол A имеет " + Rad + " радиан");

            Console.ReadKey(true);
        }
    }
}
