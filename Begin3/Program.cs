﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Begin3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Сторана прямоугольника A = ");
            double a = Convert.ToDouble(Console.ReadLine());

            Console.Write("Сторона прямоугольника B = ");
            double b = Convert.ToDouble(Console.ReadLine());

            double S = a * b;
            Console.WriteLine("Площадь прямоугольника = " + S);

            double P = 2 * (a + b);
            Console.WriteLine("Периметр прямоугольника = " + P);

            Console.ReadKey(true);
        }
    }
}
