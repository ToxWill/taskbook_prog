﻿using System;

namespace Begin30
{
    class Program
    {
        static void Main(string[] args)
        {
            double pi, Grad, Rad;

            Console.Write("Ввдеите радианную меру угла (0 <= X < 6,28) - ");
            Rad = Convert.ToDouble(Console.ReadLine());

            pi = 3.14;

            Grad = 180 * (Rad / pi);

            Console.WriteLine("Угол " + Rad + " радиан составляет " + Grad + " градусов");

            Console.ReadKey(true);
        }
    }
}
