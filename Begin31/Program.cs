﻿using System;

namespace Begin31
{
    class Program
    {
        static void Main(string[] args)
        {
            double Gc, Gf;

            Console.Write("Введите значения температуры в градусах по Фаренгейту - ");
            Gf = Convert.ToDouble(Console.ReadLine());

            Gc = (Gf - 32) * 1.8;

            Console.WriteLine("Если показатели градусов равняеятся " + Gf + " по Фаренгейту, то по Цельсию будет равна = " + Gc);

            Console.ReadKey(true);
        }
    }
}
