﻿using System;

namespace Begin32
{
    class Program
    {
        static void Main(string[] args)
        {
            double Gc, Gf;

            Console.Write("Введите значения температуры в градусах по Цельсию - ");
            Gc = Convert.ToDouble(Console.ReadLine());

            Gf = 1.8 * Gc + 32;

            Console.WriteLine("Если показатели градусов равняеятся " + Gc + " по Цельсию, то по Фаренгейту будет равна = " + Gf);

            Console.ReadKey(true);
        }
    }
}
