﻿using System;

namespace Begin33
{
    class Program
    {
        static void Main(string[] args)
        {
            double X, Y, A;

            Console.WriteLine("Известно, что X кг конфет стоит A рублей. Определить, " +
                "сколько стоит 1 кг и Y кг этих же конфет");

            Console.Write("Введите вес конфет в килограммах - ");
            X = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите стоимость конфет в рублях - ");
            A = Convert.ToDouble(Console.ReadLine());

            Y = A / X;

            Console.WriteLine("1кг этих конфет стоят - " + Y + " рублей");

            Console.ReadKey(true);
        }
    }
}
