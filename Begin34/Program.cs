﻿using System;

namespace Begin34
{
    class Program
    {
        static void Main(string[] args)
        {
            double X, Y, B, A, K, I, Raz;

            Console.WriteLine("Известно, что X кг шоколадных конфет стоит A рублей, а Y кг ирисок " +
                "стоит B рублей. Определить, сколько стоит 1 кг шоколадных конфет, 1 кг ирисок, " +
                "а также во сколько раз шоколадные конфеты дороже ирисок");

            Console.Write("Введите вес шоколажных конфет в килограммах - ");
            X = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите стоимость шоколадных конфет в рублях - ");
            A = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите вес ирисок в килограммах - ");
            Y = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите стоимость ирисок в рублях - ");
            B = Convert.ToDouble(Console.ReadLine());

            K = A / X;
            I = B / Y;
            Raz = K / I;

            Console.WriteLine("1кг шокоалдных конфет стоят - " + K + " рублей");
            Console.WriteLine("1кг ирисок стоят - " + I + " рублей");
            Console.WriteLine("Шоколадные конфеты дороже ирисок в " + Raz + " раз");

            Console.ReadKey(true);
        }
    }
}
