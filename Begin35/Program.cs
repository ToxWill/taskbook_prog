﻿using System;

namespace Begin35
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Скорость лодки в стоячей воде V км/ч, скорость течения реки U км/ч " +
                "(U < V). Время движения лодки по озеру T1 ч, а по реке (против течения) — T2 ч. " +
                "Определить путь S, пройденный лодкой (путь = время · скорость). Учесть, что при " +
                "движении против течения скорость лодки уменьшается на величину скорости течения. ");

            double V, U, S, T1, T2;

            Console.Write("Введите скокрость лодки в стоячей воде - ");
            V = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите скорость течения вводы - ");
            U = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите время движения лодки по озеру - ");
            T1 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите ввремя движения лодки по реке - ");
            T2 = Convert.ToDouble(Console.ReadLine());

            S = (T1 * V) + (T2 - (V - U));

            Console.WriteLine("Путь пройденный лодкой = " + S);

            Console.ReadKey(true);        }
    }
}
