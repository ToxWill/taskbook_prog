﻿using System;

namespace Begin38
{
    class Program
    {
        static void Main(string[] args)
        {
            double A, B, X;

            Console.WriteLine("Решить линейное уравнение A·x + B = 0, заданное своими коэффициентами " +
                "A и B (коэффициент A не равен 0).");

            Console.Write("Ввидете значение А - ");
            A = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите значение B - ");
            B = Convert.ToDouble(Console.ReadLine());

            X = -(B / A);

            Console.WriteLine("Результат X = " + X);
            Console.ReadKey(true);
        }
    }
}
