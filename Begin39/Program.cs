﻿using System;

namespace Begin39
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Найти корни квадратного уравнения A*x^2 + B*x + C = 0, заданного " +
                "своими коэффициентами A, B, C (A > 0), если известно, что дискриминант уравнения " +
                "положителен. Вывести вначале меньший, а затем больший из найденных корней. Корни " +
                "квадратного уравнения находятся по формуле x1, 2 = (-B ± (D)1 / 2) / (2·A), где " +
                "D — дискриминант, равный B^2 - 4*A*C. ");

            double A, B, C, D, X1, X2;

            Console.Write("Введите значение A - ");
            A = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите значение B - ");
            B = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите значение C - ");
            C = Convert.ToDouble(Console.ReadLine());

            D = Math.Pow(B, 2) - 4 * A * C;

            X1 = (-B - Math.Sqrt(D)) / (2 * A);
            X2 = (-B + Math.Sqrt(D)) / (2 * A);

            if (X1 <= X2)
            {
                Console.WriteLine("Дискриминант равен " + D);
                Console.WriteLine("Первый корень = " + X1 + "; Второй корень = " + X2);
            } 

            else

            {
                Console.WriteLine("Дискриминант равен " + D);
                Console.WriteLine("Первый корень = " + X2 + "; Второй корень = " + X1);
            }

            Console.ReadKey(true);
        }
    }
}
