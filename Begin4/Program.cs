﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Begin4
{
    class Program
    {
        static void Main(string[] args)
        {
            double d, p, L;

            Console.Write("Введите диаметр окружности = ");
            d = Convert.ToDouble(Console.ReadLine());

            p = 3.14;

            L = d * p;

            Console.WriteLine("Длина окружности = {0}", L);
            Console.ReadKey(true);
        }
    }
}
