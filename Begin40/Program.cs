﻿using System;

namespace Begin40
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Найти решение системы линейных уравнений вида A1*x + B1*y = C1, " +
                "A2*x + B2*y = C2, заданной своими коэффициентами A1, B1, C1, A2, B2, C2, если " +
                "известно, что данная система имеет единственное решение. Воспользоваться " +
                "формулами x = (C1*B2 - C2*B1) / D, y = (A1*C2 - A2*C1)/ D, где D = A1*B2 - A2*B1");

            double x, y, D, A1, B1, C1, A2, B2, C2;

            Console.WriteLine("Ввдеите значения первых коэффициентов:");
            Console.Write("A1 = ");
            A1 = Convert.ToDouble(Console.ReadLine());
            Console.Write("B1 = ");
            B1 = Convert.ToDouble(Console.ReadLine());
            Console.Write("C1 = ");
            C1 = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Ввдеите значения вторых коэффициентов:");
            Console.Write("A2 = ");
            A2 = Convert.ToDouble(Console.ReadLine());
            Console.Write("B2 = ");
            B2 = Convert.ToDouble(Console.ReadLine());
            Console.Write("C2 = ");
            C2 = Convert.ToDouble(Console.ReadLine());

            D = A1 * B2 - A2 * B1;

            x = (C1 * B2 - C2 * B1) / D;
            y = (A1 * C2 - A2 * C1) / D;

            Console.WriteLine("Результат:");
            Console.WriteLine("D = " + D);
            Console.WriteLine("X = " + x);
            Console.WriteLine("Y = " + y);

            Console.ReadKey(true);
        }
    }
}
