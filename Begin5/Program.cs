﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Begin5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите длину ребра куба = ");
            int a = int.Parse(Console.ReadLine());

            int V = (int)Math.Pow(a,3);
            int S = 6 * (int)Math.Pow(a, 2);

            Console.WriteLine("Объем куба = " + V);
            Console.WriteLine("Площадь куба = " + S);

            Console.ReadKey(true);
        }
    }
}
