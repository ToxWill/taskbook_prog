﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Begin6
{
    class Program
    {
        static void Main(string[] args)
        {
            double a, b, c, S, V;

            Console.Write("Введите длину ребра прямогульника параллелепипеда A = ");
            a = Convert.ToDouble(Console.ReadLine());
            Console.Write("Введите длину ребра прямогульника параллелепипеда B = ");
            b = Convert.ToDouble(Console.ReadLine());
            Console.Write("Введите длину ребра прямогульника параллелепипеда C = ");
            c = Convert.ToDouble(Console.ReadLine());

            V = a * b * c;
            S = 2 * (a * b + b * c + a * c);

            Console.WriteLine("Объем прямоугольного параллелепипеда = " + V);
            Console.WriteLine("Площадь поверхности прямоугольного параллелепипеда = " + S);

            Console.ReadKey(true);
        }
    }
}
