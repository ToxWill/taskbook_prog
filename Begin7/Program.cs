﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Begin7
{
    class Program
    {
        static void Main(string[] args)
        {
            double p, r, L, S;

            Console.Write("Введите радиус окружности = ");
            r = Convert.ToDouble(Console.ReadLine());

            p = 3.14;

            L = 2 * p * r;
            S = p * (double)Math.Pow(r, 2);

            Console.WriteLine("Длина окружности = " + L);
            Console.WriteLine("Площадь круга = " + S);

            Console.ReadKey(true);
        }
    }
}
