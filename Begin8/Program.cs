﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Begin8
{
    class Program
    {
        static void Main(string[] args)
        {
            double a, b, Sa;

            Console.Write("Введите первое число - ");
            a = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите второе число - ");
            b = Convert.ToDouble(Console.ReadLine());

            Sa = (a + b) / 2;

            Console.WriteLine("Среднее арифметическое этих чисел равна " + Sa);

            Console.ReadKey(true);
        }
    }
}
