﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Begin9
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b;

            Console.Write("Введите первое число - ");
            a = Convert.ToInt32(Console.ReadLine());

            Console.Write("Введите второе число - ");
            b = Convert.ToInt32(Console.ReadLine());

            double Sg = Math.Sqrt(Math.Abs(a) * Math.Abs(b));

            Console.WriteLine("Среднее геометрическое этих чисел равна " + Sg);

            Console.ReadKey(true);
        }
    }
}
