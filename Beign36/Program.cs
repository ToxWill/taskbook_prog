﻿using System;

namespace Beign36
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Скорость первого автомобиля V1 км/ч, второго — V2 км/ч, расстояние между" +
                " ними S км. Определить расстояние между ними через T часов, если автомобили " +
                "удаляются друг от друга, двигаясь в противоположных направлениях. Данное " +
                "расстояние равно сумме начального расстояния и общего пути, проделанного " +
                "автомобилями; общий путь = время · суммарная скорость.");

            double V1, V2, S, T, Summ;

            Console.Write("Введите скорость первого автомобиля - ");
            V1 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите скорость второго автомобиля - ");
            V2 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите расстояние между ними в километрах - ");
            S = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите время отдаления автомобилей в часах - ");
            T = Convert.ToDouble(Console.ReadLine());

            Summ = S + T * (V1 + V2);

            Console.Write("Расстояние между автомобилями равна " + Summ);

            Console.ReadKey(true);
        }
    }
}
