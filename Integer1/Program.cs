﻿using System;

namespace Integer1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Дано расстояние L в сантиметрах. Используя " +
                "операцию деления нацело, найти количество полных метров в " +
                "нем (1 метр = 100 см)");

            int L, M, Sm;

            Console.Write("Введите расстояние в сантиметрах - ");
            L = Convert.ToInt32(Console.ReadLine());

            M = L / 100;
            Sm = L % 100;

            Console.WriteLine("В " + L + " сантиметрах будует полных " + M + " метра и " + Sm + " сантиметра");
            Console.ReadKey(true);
        }
    }
}
