﻿using System;

namespace Integer2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Дана масса M в килограммах. Используя операцию " +
                "деления нацело, найти количество полных тонн в ней " +
                "(1 тонна = 1000 кг).");

            int M, T, Kg;

            Console.Write("Введите массу в килограммах - ");
            M = Convert.ToInt32(Console.ReadLine());

            T = M / 1000;
            Kg = M % 1000;

            Console.WriteLine("В массе " + M + " будует полных " + T + " тонна и " + Kg + " килограмма");
            Console.ReadKey(true);
        }
    }
}
