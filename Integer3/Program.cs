﻿using System;

namespace Integer3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Дан размер файла в байтах. Используя операцию " +
                "деления нацело, найти количество полных килобайтах, которые " +
                "занимает данный файл (1 килобайт = 1024 байта).");

            int Razmer, bayt, kilbayt;

            Console.Write("Введите размер файла в байтах - ");
            Razmer = Convert.ToInt32(Console.ReadLine());

            kilbayt = Razmer / 1024;
            bayt = Razmer % 1024;

            Console.WriteLine("В " + Razmer + " байтах будет полных " + kilbayt + " килобайта и " + bayt + 
                " байтов");
            Console.ReadKey(true);
        }
    }
}
