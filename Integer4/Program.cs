﻿using System;

namespace Integer4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Даны целые положительные числа А и В (А > B). " +
                "На отрезке длины А размещено максимально возможное количество " +
                "отрезков длины В (без наложений). Используя операцию деления " +
                "нацело, найти количество отрезков В, размещенных на отрезке А.");

            int A, B, otr;

            Console.WriteLine("Введите положительные числа А и В (А > В):");
            Console.Write("Число А = ");
            A = Convert.ToInt32(Console.ReadLine());
            Console.Write("Число В = ");
            B = Convert.ToInt32(Console.ReadLine());

            otr = A / B;

            Console.WriteLine("В отрезке А, разположенно " + otr + " полных отрезков");
            Console.ReadKey(true);
        }
    }
}
